package Labs.Lab1;

import org.hibernate.annotations.DiscriminatorOptions;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import java.util.Random;
import java.util.UUID;

@Entity
public class T_Shirt extends Product {

    private static final String[] TSHIRTS_NAMES = new String[]{"red", "blue", "yellow", "green", "pink"};
    private static final String[] TSHIRTS_COMPANY_NAMES = new String[]{"KZ", "Russian", "GoshaRubchinsky", "China"};

    public T_Shirt() {
        super();
        productID = UUID.randomUUID();
    }

    public T_Shirt(UUID id) {
        super();
        productID = id;
    }

    @Override
    public void create() {
        Random r = new Random();
        //this.productID=UUID.randomUUID();
        this.productName = TSHIRTS_NAMES[r.nextInt(TSHIRTS_NAMES.length)];
        this.price = 100 + r.nextInt(1000) * 100;
        this.productMakerName = TSHIRTS_COMPANY_NAMES[r.nextInt(TSHIRTS_COMPANY_NAMES.length)];
    }

    @Override
    public void read() {
        System.out.println("Футболка:: productID: " + productID + " Имя продукта: " + productName + " Цена:" + price + " Имя производителя: " + productMakerName);
    }

    @Override
    public String getInfo() {
        return "Футболка:: productID: " + productID + " Имя продукта: " + productName + " Цена:" + price + " Имя производителя: " + productMakerName;
    }
}
