package Labs.Lab1;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import java.util.Random;
import java.util.UUID;

@Entity
public class Cap extends Product {

    private static final String[] CAP_NAMES = new String[]{"FBI", "MOSCOW", "LONDON", "BestCap", "simple cap"};
    private static final String[] CAP_COMPANY_NAMES = new String[]{"H&M", "Lacoste", "GoshaRubchinsky", "China"};


    public Cap() {
        super();
        productID = UUID.randomUUID();
    }

    public Cap(UUID id) {
        super();
        productID = id;
    }

    public void create() {
        Random r = new Random();
        this.productID = UUID.randomUUID();
        this.productName = CAP_NAMES[r.nextInt(CAP_NAMES.length)];
        this.price = 100 + r.nextInt(1000) * 100;
        this.productMakerName = CAP_COMPANY_NAMES[r.nextInt(CAP_COMPANY_NAMES.length)];
    }


    public void read() {
        System.out.println("Кепка:: productID: " + productID + " Имя продукта: " + productName + " Цена:" + price + " Имя производителя: " + productMakerName);

    }

    @Override
    public String getInfo() {
        return "Кепка:: productID: " + productID + " Имя продукта: " + productName + " Цена:" + price + " Имя производителя: " + productMakerName;
    }
}
