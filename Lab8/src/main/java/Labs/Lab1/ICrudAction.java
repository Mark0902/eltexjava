package Labs.Lab1;

public interface ICrudAction {
    void create();

    void read();

    void update();

    void delete();

    String getInfo();
}
