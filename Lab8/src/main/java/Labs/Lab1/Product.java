package Labs.Lab1;

import Labs.Lab2.Order;

import javax.persistence.Entity;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.Scanner;
import java.util.UUID;

@Entity
@Table(name = "products")
public class Product implements ICrudAction, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_product")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "UUID")
    UUID productID;
    @Column(name = "productName")
    String productName;
    @Column(name = "price")
    double price;

    public Order getOrders() {
        return orders;
    }

    public void setOrders(Order orders) {
        this.orders = orders;
    }

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id")
    private Order orders;

    //не в БД

    @Column(name = "productMakerName")
    String productMakerName;

    public Product() {
    }

    public Product(String productName, double price, String productMakerName) {
        this.productID = UUID.randomUUID();
        this.productName = productName;
        this.price = price;
        this.productMakerName = productMakerName;
    }


    public void create() {
    }

    public void read() {
    }

    public String getInfo() {
        return "";
    }

    public UUID getProductID() {
        return productID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setProductID(UUID productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProductMakerName() {
        return productMakerName;
    }

    public void setProductMakerName(String productMakerName) {
        this.productMakerName = productMakerName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id &&
                Objects.equals(productID, product.productID);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, productID);
    }

    public void update() {
        Scanner scanner = new Scanner(System.in);
        //System.out.println("Ввод ID-товара ");
        this.productID = UUID.randomUUID();
        System.out.println("Ввод названия товара ");
        this.productName = scanner.next();
        System.out.println("Ввод цены товара ");

        this.price = scanner.nextDouble();
        System.out.println("Ввод фирмы-производителя ");
        this.productMakerName = scanner.next();
    }

    public void delete() {
        //this.productID=;
        this.productName = "";
        this.price = 0;
        this.productMakerName = "";

    }
}
