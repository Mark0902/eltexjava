package Labs.Lab3;

import Labs.Lab1.Product;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

public class GenericShoppingCart<T> {

    private ArrayList<T> arrayList;
    private Set<UUID> uuidSet;

    public GenericShoppingCart() {
        this.arrayList = new ArrayList<>();
        this.uuidSet = new TreeSet<>();
    }

    public void add(T product) {
        arrayList.add(product);
    }

    public void delete(Product product) {
        arrayList.remove(product);
    }

    public ArrayList<T> getArrayList() {
        return arrayList;
    }

    public boolean checkProductByUUID(UUID uuid) {
        return uuidSet.contains(uuid);
    }

    public void showAllProducts() {
        for (T product : arrayList) {
            System.out.print(product.toString() + " ");
        }
        System.out.print("\n");
    }

}
