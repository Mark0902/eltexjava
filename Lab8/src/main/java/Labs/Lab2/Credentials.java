package Labs.Lab2;

import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Random;

@Entity
@Table(name = "credentials")
public class Credentials implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final String[] emails = new String[]{"s1@gmail.com", "s2@gmail.com", "s3@gmail.com", "s4@gmail.com", "s5@gmail.com", "s6@gmail.com", "s7@gmail.com", "s8@gmail.com"};

    @Id
    @Column(name = "id_credentials")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ID;
    @Column(name = "surname")
    private String surname;
    @Column(name = "name")
    private String name;
    @Column(name = "patronymic")
    private String patronymic;
    @Column(name = "email")
    private String email;


    @OneToMany(mappedBy = "credentials", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<Order> orders;

    public Collection<Order> getOrders() {
        return orders;
    }

    public void setOrders(Collection<Order> orders) {
        this.orders = orders;
    }

    public Credentials(String surname, String name, String patronymic, String email) {
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.email = email;
    }

    public static Credentials generateCredentials() {
        Random random = new Random();
        return new Credentials("Александров", "Александр", "Александрович", emails[random.nextInt(emails.length)]);

    }

    public String infoString() {
        return ID + " " + surname + " " + name + " " + patronymic + " (" + email + ")";

    }

    public Credentials() {
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
