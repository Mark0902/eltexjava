package Labs.Lab2;

import Labs.Lab1.Product;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

public class ShoppingCart {

    private ArrayList<Product> arrayList;
    private Set<UUID> uuidSet;

    public ShoppingCart() {
        this.arrayList = new ArrayList<>();
        this.uuidSet=new TreeSet<>();
        //this.ID =id;
    }

    public boolean add(Product product) {
        if (!uuidSet.contains(product.getProductID())) {
            arrayList.add(product);
            uuidSet.add(product.getProductID());
            return true;
        }
        else return false;
    }

    public void delete(Product product){
        arrayList.remove(product);
    }

    public ArrayList<Product> getArrayList() {
        return arrayList;
    }

    public boolean checkProductByUUID(UUID uuid){
        return uuidSet.contains(uuid);
    }


    public void showAllProducts(){
        for(Product product : arrayList){
            product.read();
        }
    }

}
