package Labs.Lab2;


import Labs.Lab1.Product;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Entity
@Table(name = "orders")
public class Order implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_order")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ID;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "credentials_id")
    private Credentials credentials;

    @Column(name = "timeOfCreating")
    private LocalDateTime timeOfCreating;

    @Column(name = "waitingTimeInHours")
    private int waitingTimeInHours;

    @OneToMany(mappedBy = "orders", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Product> products;


    //Value (autoApply = true) means that @Converter is automatically used for conversion of every LocalDateTime property in your JPA Entity.
    @Converter(autoApply = true)
    public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Timestamp> {

        @Override
        public Timestamp convertToDatabaseColumn(LocalDateTime locDateTime) {
            return (locDateTime == null ? null : Timestamp.valueOf(locDateTime));
        }

        @Override
        public LocalDateTime convertToEntityAttribute(Timestamp sqlTimestamp) {
            return (sqlTimestamp == null ? null : sqlTimestamp.toLocalDateTime());
        }
    }


    private static final int[] WAITING_TIME_EXAMPLES = {50, 20, 30, 40, 10, 60, 70, 120};


    public Order(Credentials credentials, ShoppingCart cart) {
        this.status = Status.WAITING;
        this.credentials = credentials;
        this.timeOfCreating = LocalDateTime.now();
        Random random = new Random();
        this.waitingTimeInHours = WAITING_TIME_EXAMPLES[random.nextInt(WAITING_TIME_EXAMPLES.length)];
        this.products = (ArrayList) cart.getArrayList().clone();
        this.ID = random.nextInt(1000) + 1;
    }

    private Order() {

    }

    public Credentials getCredentials() {
        return credentials;
    }

    public Order(Status status, Credentials credentials, List<Product> products) {
        this.status = status;
        this.credentials = credentials;
        this.timeOfCreating = LocalDateTime.now();
        this.waitingTimeInHours = 0;
        this.products = products;
        Random random = new Random();
        this.ID = random.nextInt(1000) + 1;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public List<Product> getProducts() {
        return products;
    }

    public LocalDateTime getTimeOfCreating() {
        return timeOfCreating;
    }

    public int getWaitingTimeInHours() {
        return waitingTimeInHours;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }


    public void printInfo() {
        System.out.println("ЗАКАЗ: " + ID + " " + credentials.infoString() + " ДАТА СОЗДАНИЯ :" + timeOfCreating.toString() + " ВРЕМЯ ОЖИДАНИЯ "
                + waitingTimeInHours + " часов, Статус: " + status + " Количество товаров " + products.size());
    }

    public String getStringInfo() {
        return "ЗАКАЗ: " + ID + "/cred/" + credentials.infoString() + " ДАТА СОЗДАНИЯ :" + timeOfCreating.toString() + " ВРЕМЯ ОЖИДАНИЯ "
                + waitingTimeInHours + " часов, Статус: " + status + " Количество товаров " + products.size();
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setTimeOfCreating(LocalDateTime timeOfCreating) {
        this.timeOfCreating = timeOfCreating;
    }

    public void setWaitingTimeInHours(int waitingTimeInHours) {
        this.waitingTimeInHours = waitingTimeInHours;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
