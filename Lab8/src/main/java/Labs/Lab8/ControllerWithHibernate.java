package Labs.Lab8;

import Labs.Lab1.Cap;
import Labs.Lab1.Product;
import Labs.Lab1.T_Shirt;
import Labs.Lab2.Order;
import Labs.Lab2.Orders;
import Labs.Lab2.ShoppingCart;
import Labs.Lab4.OrderGenerationThread;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class ControllerWithHibernate {
    ShoppingCart shoppingCart = new ShoppingCart();
    private LogManager logManager;
    private Logger logger = LogManager.getRootLogger();
    DBService dbService = new DBService();


    @RequestMapping(params = "command=readAll")
    public String readAll() {

        Orders orders = new Orders();
        OrderGenerationThread orderGenerationThread = new OrderGenerationThread(orders, 1);
        orderGenerationThread.start();
        try {
            orderGenerationThread.join(2000);
            orderGenerationThread.interrupt();
        } catch (Exception exc) {
            System.out.println(exc.getMessage());
        }


        for (Order order : orders.getOrderList()) {
            dbService.saveOrder(order);
        }


        StringBuilder stringBuilder = new StringBuilder();


        stringBuilder.append("<html><body>Все заказы из базы: <br>");
        for (Order order : dbService.getAllOrders()) {
            stringBuilder.append(order.getStringInfo());
            stringBuilder.append("<br>");
        }
        //logger.log(Level.INFO, "Заказы в JSON были показаны");
        //logger.info("Заказы показанЫ");

        return new String(stringBuilder);
    }

    @RequestMapping(params = {"command=readById", "order_id"})
    public String readById(@RequestParam(value = "order_id") int id) {

        try {
            Order order = dbService.getOrderById(id);
            return order.getStringInfo();
        } catch (NullPointerException npe) {
            return "Заказа с данным ID не сушествует";
        } catch (Exception exc) {
            return exc.getMessage();
        }
    }

    @RequestMapping(params = {"command=addToCard", "card_id"})
    public String addToCard(@RequestParam(value = "card_id") int id) {
        Random r = new Random();
        Product product;
        if (r.nextInt(2) == 1) {
            product = new Cap();
            product.create();
        } else {
            product = new T_Shirt();
            product.create();
        }
        try {
            dbService.addProductToOrder(id, product);
            return "Успешное добавление";
        } catch (NullPointerException npe) {
            return "Заказа с данным ID не сушествует";
        } catch (Exception exc) {
            return "Ошибка добавления " + exc.getMessage();
        }
    }

    @RequestMapping(params = {"command=delById", "order_id"})
    public String delById(@RequestParam(value = "order_id") int id) {
        try {
            dbService.deleteOrderById(id);
            return "Успешное удаление";
        } catch (NullPointerException npe) {
            return "Заказа с данным ID не сушествует";
        } catch (Exception exc) {
            return "Ошибка удаления";
        }
    }

}
