package Labs.Lab8;

import Labs.Lab1.Cap;
import Labs.Lab1.Product;
import Labs.Lab1.T_Shirt;
import Labs.Lab2.Credentials;
import Labs.Lab2.Order;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.service.ServiceRegistry;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class DBService {
    private static final String hibernate_show_sql = "true";
    private final SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public DBService() {
        Configuration configuration = getMySqlConfiguration();
        configuration.addAnnotatedClass(Product.class);
        sessionFactory = createSessionFactory(configuration);

        /*
        Cache cache = sessionFactory.getCache();
        if (cache != null) {
            cache.evictAllRegions();
        }
        */
    }


    private Configuration getMySqlConfiguration() {
        Configuration configuration = new Configuration();

        configuration.addAnnotatedClass(Product.class);
        configuration.addAnnotatedClass(Cap.class);
        configuration.addAnnotatedClass(T_Shirt.class);
        configuration.addAnnotatedClass(Order.class);
        configuration.addAnnotatedClass(Credentials.class);

        Properties properties = new Properties();
        try {
            FileInputStream fis = new FileInputStream("src/main/resources/bdSettings.properties");
            properties.load(fis);
            String a = properties.getProperty("dialect");
            String pass = new String(new BASE64Decoder().decodeBuffer(properties.getProperty("password")), 0);

            configuration.setProperty("hibernate.dialect", properties.getProperty("dialect"));
            configuration.setProperty("hibernate.connection.driver_class", properties.getProperty("driver_class"));
            configuration.setProperty("hibernate.connection.url", properties.getProperty("url"));
            configuration.setProperty("hibernate.connection.username", properties.getProperty("username"));
            configuration.setProperty("hibernate.connection.password", pass);
            //Вывод в консоль
            configuration.setProperty("hibernate.show_sql", "true");
            configuration.setProperty("hibernate.hbm2ddl.auto", "update");

            configuration.setProperty("hibernate.connection.CharSet", properties.getProperty("CharSet"));
            configuration.setProperty("hibernate.connection.characterEncoding", properties.getProperty("CharacterEncoding"));
            configuration.setProperty("hibernate.connection.useUnicode", properties.getProperty("useUnicode"));
        } catch (Exception exc) {
            System.out.println(exc.getMessage());
        }
        
        return configuration;
    }

    public Product getProductById(long id) {
        Session session = sessionFactory.openSession();
        Product pr = session.get(Product.class, id);
        session.close();
        return pr;
    }

    public void saveOrder(Order order) {
        Session session = sessionFactory.openSession();
        Credentials credentials = order.getCredentials();
        Query query = session.createQuery("from Credentials where email='" + credentials.getEmail() + "'");
        List list = query.list();
        /*
        if (list.size() == 0) {
          //  session.save(credentials);
        }
        */

        for (Product product : order.getProducts()) {
            product.setOrders(order);
        }

        session.beginTransaction();
        //session.saveOrUpdate(credentials);
        session.save(order);
        //session.save(order);
        session.flush();
        session.getTransaction().commit();
        session.close();
    }

    public Order getOrderById(long id) {
        Session session = sessionFactory.openSession();
        Order order = session.get(Order.class, id);
        session.close();
        return order;
    }


    public List<Order> getAllOrders() {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("from Order");
        List<Order> list = query.list();
        session.close();
        return list;
    }

    public void deleteOrderById(long id) {

        Session session = sessionFactory.openSession();
        Order order = getOrderById(id);
        session.beginTransaction();
        session.delete(order);
        session.flush();
        session.getTransaction().commit();
        session.close();

    }

    public void addProductToOrder(long id, Product product) {

        Session session = sessionFactory.openSession();

        Order order = getOrderById(id);
        order.getProducts().add(product);
        product.setOrders(order);

        session.beginTransaction();
        session.saveOrUpdate(order);
        session.flush();
        session.getTransaction().commit();
        session.close();
    }

    /*
    public UsersDataSet getUser(long id) throws DBException {
        try {
            Session session = sessionFactory.openSession();
            UsersDAO dao = new UsersDAO(session);
            UsersDataSet dataSet = dao.get(id);
            session.close();
            return dataSet;
        } catch (HibernateException e) {
            throw new DBException(e);
        }
    }

    public long addUser(String name) throws DBException {
        try {
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            UsersDAO dao = new UsersDAO(session);
            long id = dao.insertUser(name);
            transaction.commit();
            session.close();
            return id;
        } catch (HibernateException e) {
            throw new DBException(e);
        }
    }


    public void printConnectInfo() {
        try {
            SessionFactoryImpl sessionFactoryImpl = (SessionFactoryImpl) sessionFactory;
            Connection connection = sessionFactoryImpl.getConnectionProvider().getConnection();
            System.out.println("DB name: " + connection.getMetaData().getDatabaseProductName());
            System.out.println("DB version: " + connection.getMetaData().getDatabaseProductVersion());
            System.out.println("Driver: " + connection.getMetaData().getDriverName());
            System.out.println("Autocommit: " + connection.getAutoCommit());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

*/
    private static SessionFactory createSessionFactory(Configuration configuration) {
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
        builder.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = builder.build();
        return configuration.buildSessionFactory(serviceRegistry);
    }
}
