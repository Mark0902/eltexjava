package Labs.Lab5;

import Labs.Lab2.Order;
import Labs.Lab7.DeleteOrderException;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.MalformedJsonException;

import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class ManagerOrderJSON extends AManagerOrder {

    public ManagerOrderJSON(String path, String objName) {
        super(path, objName);
    }

    @Override
    public Order readById(int ID) throws DeleteOrderException {
        File file = new File(path + objName + ID + ".txt");
        Gson gson = new Gson();
        if (file.exists()) {
            try (JsonReader jsonReader = new JsonReader(new FileReader(file))) {
                Order order = gson.fromJson(jsonReader, Order.class);
                return order;
            } catch (MalformedJsonException | JsonSyntaxException | EOFException eofException) {
                throw new DeleteOrderException("Поврежденный JSON", 2);
            } catch (Exception exc) {
                System.out.println(exc.getMessage());

            }
        } else {
            throw new DeleteOrderException("НЕ НАЙДЕН заказ в формате JSON", 1);
            //System.out.println("НЕ НАЙДЕН заказ в формате JSON");
        }
        return null;
    }

    @Override
    public void saveById(Order order, int ID) {
        Gson gson = new Gson();
        try (JsonWriter jsonWriter = new JsonWriter(new FileWriter(path + objName + ID + ".txt"))) {
            gson.toJson(order, Order.class, jsonWriter);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public List<Order> readAll() {
        List<Order> ordersFromFile = new ArrayList<>();
        Gson gson = new Gson();
        File file;
        for (int i = 1; i < 5000; i++) {
            file = new File(path + objName + i + ".txt");
            if (file.exists()) {
                try (JsonReader jsonReader = new JsonReader(new FileReader(file))) {
                    ordersFromFile.add(gson.fromJson(jsonReader, Order.class));
                } catch (Exception exc) {
                    System.out.println(exc.toString());
                }
            }
        }
        return ordersFromFile;
    }


    @Override
    public void saveAll(List<Order> ordersToSave) {
        Gson gson = new Gson();
        for (Order order : ordersToSave) {
            long ID = order.getID();
            try (JsonWriter jsonWriter = new JsonWriter(new FileWriter(path + objName + ID + ".txt"))) {
                gson.toJson(order, Order.class, jsonWriter);

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }
    }

    public void deleteById(int ID) throws DeleteOrderException {
        File file = new File(path + objName + ID + ".txt");
        if (file.exists()) {
            file.delete();
            throw new DeleteOrderException("Успешное удаление", 0);
        } else {
            throw new DeleteOrderException("НЕ НАЙДЕН заказ в формате JSON", 1);
        }

    }
}


