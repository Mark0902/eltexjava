package Labs.Lab5;

import Labs.Lab2.Order;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ManagerOrderFile extends AManagerOrder {

    public ManagerOrderFile(String path,String objName) {
        super(path,objName);
    }

    @Override
    public Order readById(int ID) {
        File file = new File(path +objName + ID + ".bin");
        if (file.exists()) {
            try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
                return (Order) objectInputStream.readObject();
            } catch (Exception exc) {
                System.out.println(exc.getMessage());
            }
        }
        System.out.println("НЕ НАЙДЕН заказ в бинарном формате");
        return null;

    }

    @Override
    public void saveById(Order order, int ID) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new
                FileOutputStream(path +objName + ID + ".bin"))) {
            objectOutputStream.writeObject(order);
        } catch (Exception exc) {
            System.out.println(exc.toString());
        }
    }

    @Override
    public List<Order> readAll() {
        List<Order> ordersFromFile = new ArrayList<>();
        File file;
        for (int i = 1; i < 200; i++) {
            file = new File(path +objName + i + ".bin");
            if (file.exists()) {
                try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
                    ordersFromFile.add((Order) objectInputStream.readObject());
                } catch (Exception exc) {
                    System.out.println(exc.toString());
                }
            }
        }
        return ordersFromFile;
        //System.out.println("Найдено заказов:" + ordersFromFile.size());
    }

    @Override
    public void saveAll(List<Order> ordersToSave) {
        for (Order order : ordersToSave) {
            long ID = order.getID();
            try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new
                    FileOutputStream(path +objName + ID + ".bin"))) {
                objectOutputStream.writeObject(order);

            } catch (Exception exc) {
                System.out.println(exc.toString());
            }
        }

    }
}
