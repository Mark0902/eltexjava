/*package Labs.Lab7;

import Labs.Lab1.Cap;
import Labs.Lab1.Product;
import Labs.Lab1.T_Shirt;
import Labs.Lab2.*;
import Labs.Lab4.OrderGenerationThread;
import Labs.Lab5.ManagerOrderJSON;
import com.google.gson.Gson;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Random;

@RestController
public class MainController {

    String fileName = "order";
    ManagerOrderJSON managerOrderJSON = new ManagerOrderJSON("/home/mark/IdeaProjects/Lab7/orders/", fileName);
    ShoppingCart shoppingCart = new ShoppingCart();
    private LogManager logManager;
    private Logger logger = LogManager.getRootLogger();


    //@RequestMapping("/")
    //public String index() {
    //  return "Greetings from Spring Boot!";
    //}

    @RequestMapping(params = "command=readall")
    public String readAll() {

        Orders orders = new Orders();
        OrderGenerationThread orderGenerationThread = new OrderGenerationThread(orders, 1);
        orderGenerationThread.start();
        try {
            orderGenerationThread.join(2000);
            orderGenerationThread.interrupt();
        } catch (Exception exc) {
            System.out.println(exc.getMessage());
        }

        managerOrderJSON.saveAll(orders.getOrderList());

        Gson gson = new Gson();
        StringBuilder stringBuilder = new StringBuilder();
        List<Order> listFromJsonFiles = managerOrderJSON.readAll();
        stringBuilder.append("<html><body>Все заказы в формате JSON: <br>");
        for (Order order : listFromJsonFiles) {
            stringBuilder.append(gson.toJson(order));
            stringBuilder.append("<br>");
        }
        stringBuilder.append("</body></html>");
        logger.log(Level.INFO, "Заказы в JSON были показаны");
        logger.info("Заказы показанЫ");
        return new String(stringBuilder);
    }

    @RequestMapping(params = {"command=readById", "order_id"})
    public String readById(@RequestParam(value = "order_id") int id) {
        try {
            Order order = managerOrderJSON.readById(id);
            Gson gson = new Gson();
            logger.log(Level.INFO, "Был показан заказ с ID " + id);
            logger.info("Показываю");
            return "Заказ с ID" + id + ": " + gson.toJson(order);
        }
        catch (DeleteOrderException delOrdExc) {
            return delOrdExc.getMessage() + " (Код - " + Integer.toString(delOrdExc.getNumber()) + " )";
        }catch (Exception exc) {
            return exc.getMessage();
        }

    }

    @RequestMapping(params = {"command=addToCard", "card_id"})
    public String addToCard(@RequestParam(value = "card_id") int id) {
        Random r = new Random();
        Order order;
        try {
            order = managerOrderJSON.readById(id);
            int productCount = r.nextInt(9) + 1;
            Product product = new Cap();
            for (int i = 0; i < productCount; i++) {
                if (r.nextInt(1) == 1) {
                    product = new Cap();
                    product.create();
                } else {
                    product = new T_Shirt();
                    product.create();
                }
            }
            order.getProducts().add(product);
            managerOrderJSON.saveById(order, id);
            logger.log(Level.INFO, "В заказ с ID " + id + "был добавлен товар с UUID: " + product.getProductID().toString());
            return "В корзину добавлен заказ с UUID: " + product.getProductID().toString();
        } catch (DeleteOrderException delOrdExc) {
            return delOrdExc.getMessage() + " (Код - " + Integer.toString(delOrdExc.getNumber()) + " )";
        } catch (Exception e) {
            logger.log(Level.INFO, "Ошибка добавления" + id);
        }
        return "error";
    }

    @RequestMapping(params = {"command=delById", "order_id"})
    public String delById(@RequestParam(value = "order_id") int id) {

        //return managerOrderJSON.deleteById(id);
        logger.log(Level.INFO, "Попытка удаления заказа");
        try {
            managerOrderJSON.readById(id);
            managerOrderJSON.deleteById(id);
        } catch (DeleteOrderException delOrdExc) {
            return delOrdExc.getMessage() + " (Код - " + Integer.toString(delOrdExc.getNumber()) + " )";
        } catch (Exception e) {
            logger.log(Level.INFO, "Ошибка добавления" + id);
        }
        return "error";
    }

}
*/