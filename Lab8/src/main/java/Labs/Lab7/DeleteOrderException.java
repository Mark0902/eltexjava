package Labs.Lab7;

public class DeleteOrderException extends Exception {
    private int number;

    public int getNumber() {
        return number;
    }

    public DeleteOrderException(String message, int num) {
        super(message);
        number = num;
    }
}
