package Labs.Lab6;

import Labs.Lab2.Credentials;
import Labs.Lab2.Order;
import Labs.Lab2.Status;

import java.io.ObjectOutputStream;
import java.net.*;
import java.time.LocalTime;
import java.util.ArrayList;

public class Client implements Runnable {

    private int serverPort;
    private int UDPport;
    private Order order;
    private DatagramSocket udpSocket;
    private DatagramPacket datagramPacket;

    private boolean waitingForNotificationFlag;

    private byte[] bytes;


    public Client(int UDPport) {
        this.UDPport = UDPport;
    }

    public void threadStart() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        bytes = new byte[100];
        datagramPacket = new DatagramPacket(bytes, bytes.length);
        waitingForNotificationFlag = false;
        clientWorkCycle();
    }

    public void clientWorkCycle() {
        while (true) {
            try {
                Thread.sleep(2000);
                if (!waitingForNotificationFlag) {
                    udpSocket = new DatagramSocket(UDPport);
                    //udpSocket.setSoTimeout(10000);
                    System.out.println(String.format("Клиент (%d) ждет", UDPport));
                    udpSocket.receive(datagramPacket);
                    udpSocket.close();

                    String dataString = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                    serverPort = Integer.valueOf(dataString);
                    System.out.println(String.format("Клиент (%d) получил от сервера : %s", UDPport, dataString));
                    Thread.sleep(100);


                    Socket socket = new Socket(InetAddress.getLocalHost(), serverPort);
                    System.out.println(String.format("Клиент (%d) успешно соединился", UDPport));
                    if (socket.isConnected()) {

                        // ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                        ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

                        order = new Order(Status.WAITING, Credentials.generateCredentials(), new ArrayList<>());
                        waitingForNotificationFlag = true;
                        objectOutputStream.writeObject(order);

                        //objectInputStream.close();
                        objectOutputStream.close();
                    }
                    socket.close();

                }
                if (waitingForNotificationFlag) {
                    //Ожидание оповещения о статусе заказа
                    udpSocket = new DatagramSocket(UDPport);
                    //udpSocket.setSoTimeout(10000);
                    udpSocket.receive(datagramPacket);
                    System.out.println(String.format("Клиент (%d) ждет оповещения о статусе", UDPport));
                    udpSocket.close();
                    String notificationData = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                    if (notificationData.equals("OK")) {
                        System.out.println("Клиент(" + UDPport + "): получил оповещение " + LocalTime.now());
                        waitingForNotificationFlag = false;
                    }
                    Thread.sleep(1000);
                }


            } catch (ConnectException connectExc) {
                System.out.println("Клиент : ConnectExc " + connectExc.getMessage());
            } catch (Exception e) {
                System.out.println("Ошибка клиента" + UDPport + " " + e.getMessage() + " " + e.getCause() + " " + e.getClass());
            }
        }
    }
}
