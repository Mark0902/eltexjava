package Lab6;

import Lab2.Order;
import Lab2.Orders;

import java.io.ObjectInputStream;
import java.net.*;

public class Server implements Runnable {

    private int UDPport = 9001;
    private int portToClient = 9999;
    private int serverPort = 8888;
    private Thread thread;
    private Orders serverOrders;

    private DatagramPacket datagramPacket;
    private ServerSocket serverSocket;
    private byte[] bytes;

    private ProcessingThreadWithUDP processingThreadWithUDP;
    private Boolean notificationFlag;

    public Server(int UDPport, Orders orders) {
        this.UDPport = UDPport;
        this.serverOrders = orders;
        notificationFlag = false;
    }

    public void threadStart() {
        processingThreadWithUDP = new ProcessingThreadWithUDP(serverOrders, 2, UDPport, notificationFlag);
        processingThreadWithUDP.start();
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        bytes = Integer.toString(serverPort).getBytes();
        datagramPacket = new DatagramPacket(bytes, bytes.length);
        serverWorkCycle();
    }

    public void serverWorkCycle() {
        try {
            while (true) {

                //TODO сервер не циклится
                if (!notificationFlag) {
                    Thread.sleep(3000);
                    DatagramSocket udpSocket = new DatagramSocket();
                    udpSocket.send(new DatagramPacket(bytes, bytes.length, InetAddress.getLocalHost(), UDPport));
                    udpSocket.close();

                    String dataString = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                    System.out.println("Сервер Отправил: " + dataString);
                }
                serverSocket = new ServerSocket(serverPort);
                Socket socket = serverSocket.accept();
                if (socket.isConnected()) {
                    //System.out.println("Серве" + dataString);


                    ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                    // ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());

                    Order order = (Order) objectInputStream.readObject();
                    serverOrders.getOrderList().add(order);
                    order.printInfo();
                    objectInputStream.close();
                    notificationFlag = true;
                }
                socket.close();
                serverSocket.close();
            }

        } catch (BindException bindExc){
            System.out.println("bindException " + bindExc.getMessage() + " " + bindExc.getClass());
            serverWorkCycle();
        } catch (Exception ex) {
            System.out.println("Ошибка у сервера " + ex.getMessage() + " " + ex.getClass());
        }

    }

}

