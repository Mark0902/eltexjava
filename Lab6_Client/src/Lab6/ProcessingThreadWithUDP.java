package Lab6;

import Lab2.Order;
import Lab2.Orders;
import Lab2.Status;
import Lab4.ACheck;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ProcessingThreadWithUDP extends ACheck {

    private int UDPport;
    private byte[] bytes;
    Boolean notificationFlag;

    public ProcessingThreadWithUDP(Orders orders, double seconds, int UDPport,Boolean notificationFlag) {
        super(orders, seconds);
        this.UDPport = UDPport;
        this.notificationFlag=notificationFlag;
    }

    @Override
    public void run() {
        try {
            while (true) {
                //System.out.println("Изменение состояний в списке");
                if (orders.getOrderList().size()==0)
                    notificationFlag=false;
                for (Order order : orders.getOrderList()) {
                    if (order.getStatus() == Status.WAITING) {
                        order.setStatus(Status.PROCESSED);
                        System.out.println("Состояние изменено");
                        DatagramSocket udpSocket = new DatagramSocket();
                        bytes="OK".getBytes();
                        udpSocket.send(new DatagramPacket(bytes, bytes.length, InetAddress.getLocalHost(), UDPport));
                        notificationFlag=false;
                        udpSocket.close();
                    }
                }
                Thread.sleep((long) (1000 * seconds));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

}

