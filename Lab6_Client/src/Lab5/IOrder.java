package Lab5;

import Lab2.Order;

import java.util.List;

public interface IOrder {
    Order readById(int ID);

    void saveById(Order order, int ID);

    void readAll();

    void saveAll(List<Order> ordersToSave);
}
