package Lab5;

import Lab2.Order;

import java.util.List;
import java.util.UUID;

public interface IOrder {
    Order readById(int ID);

    void saveById(Order order, int ID);

    void readAll();

    void saveAll(List<Order> ordersToSave);
}
