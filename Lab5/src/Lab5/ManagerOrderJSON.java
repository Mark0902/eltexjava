package Lab5;

import Lab2.Order;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ManagerOrderJSON extends AManagerOrder {

    public ManagerOrderJSON(String path, String objName) {
        super(path, objName);
    }

    @Override
    public Order readById(int ID) {
        File file = new File(path + objName + ID + ".txt");
        Gson gson = new Gson();
        char[] buffer = new char[500];
        int c;
        if (file.exists()) {
            try (FileReader fileReader = new FileReader(file)) {
                while ((c = fileReader.read(buffer)) > 0) {
                    if (c < 500) {
                        buffer = Arrays.copyOf(buffer, c);
                    }
                }
                Order order = gson.fromJson(String.copyValueOf(buffer), Order.class);
                return order;
            } catch (Exception exc) {
                System.out.println(exc.getMessage());
            }
        }
        System.out.println("НЕ НАЙДЕН заказ в формате JSON");
        return null;

    }

    @Override
    public void saveById(Order order, int ID) {
        Gson gson = new Gson();
        try (FileWriter fileWriter = new FileWriter(path + objName + ID + ".txt")) {
            fileWriter.write(gson.toJson(order));

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void readAll() {
        List<Order> ordersFromFile = new ArrayList<>();
        Gson gson = new Gson();
        File file;
        int c;
        char[] bufer = new char[500];
        for (int i = 1; i < 200; i++) {
            file = new File(path + objName + i + ".txt");
            if (file.exists()) {
                try (FileReader fileReader = new FileReader(file)) {
                    while ((c = fileReader.read(bufer)) > 0) {
                        if (c < 500) {
                            bufer = Arrays.copyOf(bufer, c);
                        }
                    }
                    ordersFromFile.add(gson.fromJson(String.copyValueOf(bufer), Order.class));
                } catch (Exception exc) {
                    System.out.println(exc.toString());
                }
            }
        }
        System.out.println("Найдено заказов:" + ordersFromFile.size());
    }


    @Override
    public void saveAll(List<Order> ordersToSave) {
        Gson gson = new Gson();
        for (Order order : ordersToSave) {
            int ID = order.getID();
            try (FileWriter fileWriter = new FileWriter(path + objName + ID + ".txt")) {
                fileWriter.write(gson.toJson(order));

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }
    }
}


