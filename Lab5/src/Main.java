import Lab1.Cap;
import Lab1.Product;
import Lab1.T_Shirt;
import Lab2.*;
import Lab3.GenericOrders;
import Lab3.GenericShoppingCart;
import Lab4.OrderGenerationThread;
import Lab4.ProcessedStatusCheckThread;
import Lab4.WaitingStatusCheckThread;
import Lab5.ManagerOrderFile;
import Lab5.ManagerOrderJSON;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Orders orders = new Orders();
        OrderGenerationThread orderGenerationThread = new OrderGenerationThread(orders, 1);
        orderGenerationThread.start();
        try {
            orderGenerationThread.join(2000);
            orderGenerationThread.interrupt();
        } catch (Exception exc) {
            System.out.println(exc.getMessage());
        }

        Scanner scanner=new Scanner(System.in);
        String fileName=scanner.next();
        ManagerOrderFile managerOrderFile = new ManagerOrderFile("/home/mark/IdeaProjects/Lab5/binfiles/",fileName);
        managerOrderFile.saveAll(orders.getOrderList());
        managerOrderFile.readAll();

        System.out.println("JSON");
        ManagerOrderJSON managerOrderJSON = new ManagerOrderJSON("/home/mark/IdeaProjects/Lab5/JSONfiles/",fileName);
        managerOrderJSON.saveAll(orders.getOrderList());
        managerOrderFile.readAll();


        Order orderToFile = new Order(Status.PROCESSED, Credentials.generateCredentials(), new ArrayList<>());
        managerOrderFile.saveById(orderToFile, orderToFile.getID());
        Order orderFromFile = managerOrderFile.readById(orderToFile.getID());
        orderFromFile.printInfo();

        managerOrderJSON.saveById(orderToFile, orderToFile.getID());
        Order orderFromJSONFile = managerOrderJSON.readById(orderToFile.getID());
        orderFromJSONFile.printInfo();
    }
}
