package Lab3;

import Lab2.Credentials;

import java.util.LinkedList;
import java.util.List;

public class GenericOrders<T> {

    private List<T> linkedList;

    public GenericOrders() {
        this.linkedList = new LinkedList<>();
    }

    public void makePurchase(GenericShoppingCart shoppingCart, Credentials credentials) {

        linkedList.add((T) shoppingCart.getArrayList().clone());
    }


    public void showAllOrders() {
        for (T order : linkedList) {
            System.out.print(order.toString());
        }
        System.out.print("\n");
    }
}

