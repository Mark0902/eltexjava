package Lab2;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Orders implements Serializable {

    private CopyOnWriteArrayList<Order> orderList;
    private Map<LocalDateTime, Order> hashMap;


    public Orders() {
        this.orderList = new CopyOnWriteArrayList<>();
        this.hashMap = new HashMap<>();
    }

    public List<Order> getOrderList() {
        return orderList;
    }

    public void makePurchase(ShoppingCart shoppingCart, Credentials credentials) {

        if (shoppingCart.getArrayList().size() != 0) {
            Order order = new Order(Credentials.generateCredentials(), shoppingCart);
            orderList.add(order);
            hashMap.put(order.getTimeOfCreating(), order);
            shoppingCart.getArrayList().clear();
        } else System.out.println("Пустая корзина!");
    }

    public void deleteAllPastOrders() {
        for (Order order : orderList) {
            if (order.getStatus() == Status.PROCESSED && order.getTimeOfCreating().plusHours(order.getWaitingTimeInHours()).isBefore(LocalDateTime.now().plusMinutes(1))) {
                orderList.remove(order);
                //boolean a=order.getTimeOfCreating().plusHours(order.getWaitingTimeInHours()).isBefore(LocalDateTime.now());
                //int b=1;
            }
        }
    }

    public void showAllOrders() {
        for (Order order : orderList) {
            order.printInfo();
        }
    }

    //Для проверки
    public void addDirectly() {
        Order order = new Order(Status.PROCESSED, Credentials.generateCredentials(), new ArrayList<>());
        orderList.add(order);
    }

    public void printHashMapIncludes() {
        for (LocalDateTime localDateTime : hashMap.keySet()) {
            System.out.println("Время - " + localDateTime.toString() + " Заказы:");
            hashMap.get(localDateTime).printInfo();
        }
    }
}
