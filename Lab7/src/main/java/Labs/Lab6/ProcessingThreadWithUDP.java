package Labs.Lab6;

import Labs.Lab2.Order;
import Labs.Lab2.Orders;
import Labs.Lab2.Status;
import Labs.Lab4.ACheck;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ProcessingThreadWithUDP extends ACheck {

    private byte[] bytes;
    private Server server;

    public ProcessingThreadWithUDP(Orders orders, double seconds, Server server) {
        super(orders, seconds);
        this.server=server;
    }


    @Override
    public void run() {
        try {
            while (true) {
                //System.out.println("Изменение состояний в списке");
                if (orders.getOrderList().size()==0)
                    server.setNotificationFlag(false);
                for (Order order : orders.getOrderList()) {
                    if (order.getStatus() == Status.WAITING) {
                        order.setStatus(Status.PROCESSED);
                        System.out.println("Состояние изменено");
                        DatagramSocket udpSocket = new DatagramSocket();
                        udpSocket.setBroadcast(true);
                        bytes="OK".getBytes();
                        udpSocket.send(new DatagramPacket(bytes, bytes.length, InetAddress.getByName("255.255.255.255"),server.getUDPport()));
                        System.out.println("Отправлено оповещение на "+server.getUDPport());
                        server.setNotificationFlag(false);
                        udpSocket.close();
                    }
                }
                Thread.sleep((long) (1000 * seconds));
            }
        } catch (Exception ex) {
            System.out.println("Ошибка в потоке обработки "+ex.toString());
        }

    }

}

