package Labs.Lab6;

import Labs.Lab2.Order;
import Labs.Lab2.Orders;

import java.io.ObjectInputStream;
import java.net.*;

public class Server implements Runnable {

    private int UDPport;
    private int[] UDPports = {9000, 9100, 9200};//, 9300, 9400, 9500};
    private int portNum;
    private int serverPort;
    private Thread thread;
    private Orders serverOrders;

    private DatagramPacket datagramPacket;
    private ServerSocket serverSocket;
    private byte[] bytes;

    private ProcessingThreadWithUDP processingThreadWithUDP;
    private boolean notificationFlag;

    public Server(int serverPort, Orders orders) {
        this.UDPport = UDPports[0];
        this.serverPort = serverPort;
        this.serverOrders = orders;
        notificationFlag = false;
    }

    public void threadStart() {
        processingThreadWithUDP = new ProcessingThreadWithUDP(serverOrders, 0.5, this);
        processingThreadWithUDP.start();
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        bytes = Integer.toString(serverPort).getBytes();
        datagramPacket = new DatagramPacket(bytes, bytes.length);
        portNum = 0;
        try {
            serverSocket = new ServerSocket(serverPort);
            serverSocket.setSoTimeout(4000);
        } catch (Exception ex)  {
            System.out.println(ex.getMessage());
        }
        serverWorkCycle();
    }

    public void setNotificationFlag(boolean notificationFlag) {
        this.notificationFlag = notificationFlag;
    }

    public int getUDPport() {
        return UDPport;
    }

    public void serverWorkCycle() {
        while (true) {
            try {
                UDPport = UDPports[portNum];
                System.out.println("СЕРВЕР : Работа с портом " + UDPport);
                if (!notificationFlag) {
                    Thread.sleep(3000);
                    DatagramSocket udpSocket = new DatagramSocket();
                    udpSocket.setBroadcast(true);
                    udpSocket.send(new DatagramPacket(bytes, bytes.length, InetAddress.getByName("255.255.255.255"), UDPport));
                    udpSocket.close();

                    String dataString = new String(datagramPacket.getData(), 0, datagramPacket.getLength());
                    System.out.println("Сервер Отправил: " + dataString + " на порт " + UDPport);
                    Socket socket = serverSocket.accept();
                    if (!socket.isClosed()) {
                        ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                        Order order = (Order) objectInputStream.readObject();
                        serverOrders.getOrderList().add(order);
                        order.printInfo();
                        objectInputStream.close();
                        notificationFlag = true;

                    }
                    socket.close();
                    Thread.sleep(2000);
                }
                if (!notificationFlag) {
                    portNum++;
                    if (portNum == UDPports.length) portNum = 0;
                }

            } catch (SocketTimeoutException timeOut) {
                System.out.println("Время ожидания вышло (" + UDPport + ")");
                if (!notificationFlag) {
                    portNum++;
                    if (portNum == UDPports.length) portNum = 0;
                }
            } catch (BindException bindExc) {
                System.out.println("bindException " + bindExc.getMessage() + " " + bindExc.getClass());
                portNum++;
                if (portNum == UDPports.length) portNum = 0;
            } catch (Exception ex) {
                System.out.println("Ошибка у сервера " + ex.getMessage() + " " + ex.getClass());
                if (!notificationFlag) {
                    portNum++;
                    if (portNum == UDPports.length) portNum = 0;
                }
            }
        }
    }

}


