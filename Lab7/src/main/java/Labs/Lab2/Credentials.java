package Labs.Lab2;

import java.io.Serializable;
import java.util.Random;

public class Credentials implements Serializable {
    private int ID;
    private String surname;
    private String name;
    private String patronimyc;
    private String email;

    public Credentials(int ID, String surname, String name, String patronymic, String email) {
        this.ID = ID;
        this.surname = surname;
        this.name = name;
        this.patronimyc = patronymic;
        this.email = email;
    }

    public static Credentials generateCredentials() {
        Random random =new Random();
        int randomID=1+random.nextInt(200);
        return new Credentials(randomID, "Александров", "Александр", "Александрович", "sasha@gmail.com");

    }

    public String infoString() {
        return ID + " " + surname + " " + name + " " + patronimyc + " (" + email + ")";

    }

    public int getID() {
        return ID;
    }
}
