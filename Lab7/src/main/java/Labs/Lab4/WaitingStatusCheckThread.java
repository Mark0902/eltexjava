package Labs.Lab4;

import Labs.Lab2.Order;
import Labs.Lab2.Orders;
import Labs.Lab2.Status;


public class WaitingStatusCheckThread extends ACheck {

    public WaitingStatusCheckThread(Orders orders, double seconds) {
        super(orders, seconds);
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
        try {
            while (true) {
                //System.out.println("Удаление заказов");
                for (Order order : orders.getOrderList()) {
                    if (order.getStatus() == Status.PROCESSED) {
                        orders.getOrderList().remove(order);
                        System.out.println("Заказ удален");
                    }
                }
                Thread.sleep((long)(1000 * seconds));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }
}
