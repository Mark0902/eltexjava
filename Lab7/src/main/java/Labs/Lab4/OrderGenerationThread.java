package Labs.Lab4;

import Labs.Lab1.Cap;
import Labs.Lab1.T_Shirt;
import Labs.Lab2.Credentials;
import Labs.Lab2.Orders;
import Labs.Lab2.ShoppingCart;

import java.util.Random;

public class OrderGenerationThread extends Thread {
    private Orders orders;
    private double generationPeriod;

    public OrderGenerationThread(Orders orders, double generationPeriod) {
        this.orders=orders;
        this.generationPeriod = generationPeriod;
    }

    @Override
    public void run() {
        Random r = new Random();
        int productCount;
        try {
          //  System.out.println(Thread.currentThread().getName());
            while (true) {
                productCount = r.nextInt(9)+1;
                ShoppingCart shoppingCart = new ShoppingCart();
                for (int i = 0; i < productCount; i++) {
                    if (r.nextInt(1) == 1) {
                        Cap cap = new Cap();
                        cap.create();
                        shoppingCart.add(cap);
                    } else {
                        T_Shirt t_shirt = new T_Shirt();
                        t_shirt.create();
                        shoppingCart.add(t_shirt);
                    }
                }
                orders.makePurchase(shoppingCart,Credentials.generateCredentials());
                //System.out.println("Сгенерировано,кол-во товаров "+productCount);
                Thread.sleep((long)(1000 * generationPeriod));
            }
        } catch (Exception exc) {
            System.out.println(exc.toString());
        }
    }
}
