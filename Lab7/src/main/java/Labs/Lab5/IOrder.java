package Labs.Lab5;

import Labs.Lab2.Order;
import Labs.Lab7.DeleteOrderException;

import java.util.List;

public interface IOrder  {
    Order readById(int ID) throws DeleteOrderException;

    void saveById(Order order, int ID);

    List<Order> readAll();

    void saveAll(List<Order> ordersToSave);
}
