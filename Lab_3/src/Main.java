import Lab1.Cap;
import Lab1.Product;
import Lab1.T_Shirt;
import Lab2.Credentials;
import Lab2.Orders;
import Lab3.GenericOrders;
import Lab3.GenericShoppingCart;

public class Main {
    public static void main(String[] args) {

        GenericShoppingCart shoppingCart = new GenericShoppingCart();
        Product product1 = new Cap();
        product1.create();
        Product product2 = new T_Shirt();
        product2.create();
        Product product3 = new Cap();
        product3.create();

        System.out.println("Работа с корзиной");
        shoppingCart.add(1);
        shoppingCart.add(2);
        shoppingCart.add("String");
        shoppingCart.add(new Integer[]{1, 2, 3, 4, 5});
        shoppingCart.add(true);
        shoppingCart.add(12.22);
        shoppingCart.add(product1);
        shoppingCart.showAllProducts();
        shoppingCart.delete(12.22);


        System.out.println("\nРабота с заказами");
        GenericOrders genericOrders = new GenericOrders();
        genericOrders.makePurchase(shoppingCart, Credentials.generateCredentials());
        genericOrders.showAllOrders();

    }
}
