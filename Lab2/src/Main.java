import FirstLab.Cap;
import FirstLab.Product;
import FirstLab.T_Shirt;
import Lab2.Orders;
import Lab2.ShoppingCart;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.UUID;

public class Main {
    public static void main(String[] args) {


        ShoppingCart shoppingCart=new ShoppingCart();
        Orders orders=new Orders();
        Product product1=new Cap();
        product1.create();
        Product product2=new T_Shirt();
        product2.create();
        Product product3=new Cap();
        product3.create();

        shoppingCart.add(product1);
        shoppingCart.add(product2);
        shoppingCart.add(product3);
        System.out.println("\n ПОКАЗ ПРОДУКТОВ В КОРЗИНЕ \n");
        shoppingCart.showAllProducts();
        shoppingCart.delete(product3);
        System.out.println("\n удален 3-ий товар \n");
        shoppingCart.showAllProducts();

        System.out.println("\n ПОИСК ПО UUID \n");
        System.out.println(shoppingCart.checkProductByUUID(product1.getProductID()));
        System.out.println(shoppingCart.checkProductByUUID(UUID.randomUUID()));

        System.out.println("\n ОФОРМЛЕНИЕ ЗАКАЗА \n");
        orders.makePurchase(shoppingCart);
        orders.showAllOrders();
        orders.makePurchase(shoppingCart);
        orders.showAllOrders();

        System.out.println("\n ПРОВЕРКА УДАЛЕНИЯ \n");
        orders.addDirectly();
        orders.showAllOrders();
        System.out.println("удаление");
        orders.deleteAllPastOrders();
        orders.showAllOrders();

        System.out.println("\n ПРОВЕРКА hashMap \n");
        orders.printHashMapIncludes();

    }
}
