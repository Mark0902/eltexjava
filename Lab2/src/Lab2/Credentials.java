package Lab2;

import java.util.Deque;

public class Credentials {
    private int ID;
    private String surname;
    private String name;
    private String patronimyc;
    private String email;

    public Credentials(int ID, String surname, String name, String patronymic, String email) {
        this.ID = ID;
        this.surname = surname;
        this.name = name;
        this.patronimyc = patronymic;
        this.email = email;
    }
    public static Credentials generateCredentials() {
        return new Credentials(100,"Александров","Александр","Александрович","sasha@gmail.com");

    }
    public String infoString(){
        return ID+" "+surname+" "+name+" "+patronimyc+" ("+email+")";

    }
}
