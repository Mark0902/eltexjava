import Lab1.Cap;
import Lab1.Product;
import Lab1.T_Shirt;
import Lab2.Credentials;
import Lab2.Order;
import Lab2.Orders;
import Lab3.GenericOrders;
import Lab3.GenericShoppingCart;
import Lab4.OrderGenerationThread;
import Lab4.ProcessedStatusCheckThread;
import Lab4.WaitingStatusCheckThread;

public class Main {
    public static void main(String[] args) {

        Orders orders=new Orders();
        OrderGenerationThread orderGenerationThread=new OrderGenerationThread(orders,1);
        orderGenerationThread.start();

        OrderGenerationThread orderGenerationThread2=new OrderGenerationThread(orders,0.5);
        orderGenerationThread2.start();

        WaitingStatusCheckThread waitingStatusCheckThread=new WaitingStatusCheckThread(orders,2);
        waitingStatusCheckThread.start();

        ProcessedStatusCheckThread processedStatusCheckThread=new ProcessedStatusCheckThread(orders,4);
        processedStatusCheckThread.start();
    }
}
