package Lab2;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class Orders {

    private CopyOnWriteArrayList<Order> linkedList;
    private Map<LocalDateTime, Order> hashMap;


    public Orders() {
        this.linkedList = new CopyOnWriteArrayList<>();
        this.hashMap = new HashMap<>();
    }

    public List<Order> getLinkedList() {
        return linkedList;
    }

    public void makePurchase(ShoppingCart shoppingCart, Credentials credentials) {

        if (shoppingCart.getArrayList().size() != 0) {
            Order order = new Order(Credentials.generateCredentials(), shoppingCart);
            linkedList.add(order);
            hashMap.put(order.getTimeOfCreating(), order);
            shoppingCart.getArrayList().clear();
        } else System.out.println("Пустая корзина!");
    }

    public void deleteAllPastOrders() {
        for (Order order : linkedList) {
            if (order.getStatus() == Status.PROCESSED && order.getTimeOfCreating().plusHours(order.getWaitingTimeInHours()).isBefore(LocalDateTime.now().plusMinutes(1))) {
                linkedList.remove(order);
                //boolean a=order.getTimeOfCreating().plusHours(order.getWaitingTimeInHours()).isBefore(LocalDateTime.now());
                //int b=1;
            }
        }
    }

    public void showAllOrders() {
        for (Order order : linkedList) {
            order.printInfo();
        }
    }

    //Для проверки
    public void addDirectly() {
        Order order = new Order(Status.PROCESSED, Credentials.generateCredentials(), new ArrayList<>());
        linkedList.add(order);
    }

    public void printHashMapIncludes() {
        for (LocalDateTime localDateTime : hashMap.keySet()) {
            System.out.println("Время - " + localDateTime.toString() + " Заказы:");
            hashMap.get(localDateTime).printInfo();
        }
    }
}
