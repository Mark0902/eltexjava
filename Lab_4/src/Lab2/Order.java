package Lab2;


import Lab1.Product;
import Lab3.GenericShoppingCart;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Order {
    private Status status;
    private Credentials credentials;
    private LocalDateTime timeOfCreating;
    private int waitingTimeInHours;
    private List<Product> productList;

    private static final int[] WAITING_TIME_EXAMPLES = {50, 20, 30, 40, 10, 60, 70, 120};


    public Order(Credentials credentials, ShoppingCart cart) {
        this.status = Status.WAITING;
        this.credentials = credentials;
        this.timeOfCreating = LocalDateTime.now();
        Random random = new Random();
        this.waitingTimeInHours = WAITING_TIME_EXAMPLES[random.nextInt(WAITING_TIME_EXAMPLES.length)];
        //удалять?
        this.productList = (ArrayList) cart.getArrayList().clone();
    }

    public Order(Status status, Credentials credentials, List<Product> productList) {
        this.status = status;
        this.credentials = credentials;
        this.timeOfCreating = LocalDateTime.now();
        this.waitingTimeInHours = 0;
        this.productList = productList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public LocalDateTime getTimeOfCreating() {
        return timeOfCreating;
    }

    public int getWaitingTimeInHours() {
        return waitingTimeInHours;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void printInfo() {
        System.out.println("ЗАКАЗ: " + credentials.infoString() + " ДАТА СОЗДАНИЯ :" + timeOfCreating.toString() + " ВРЕМЯ ОЖИДАНИЯ "
                + waitingTimeInHours + " часов, Статус: " + status + " Количество товаров " + productList.size());
    }
}
