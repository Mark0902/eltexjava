package Lab4;

import Lab2.Order;
import Lab2.Orders;
import Lab2.Status;


public class WaitingStatusCheckThread extends ACheck {

    public WaitingStatusCheckThread(Orders orders, double seconds) {
        super(orders, seconds);
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
        try {
            while (!isInterrupted()) {
                //System.out.println("Удаление заказов");
                for (Order order : orders.getLinkedList()) {
                    if (order.getStatus() == Status.PROCESSED) {
                        orders.getLinkedList().remove(order);
                        System.out.println("Заказ удален");
                    }
                }
                Thread.sleep((long)(1000 * seconds));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }
}
