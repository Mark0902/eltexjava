import Lab2.Orders;
import Lab6.Client;
import Lab6.Server;

import java.net.InetAddress;


public class Main {
    public static void main(String[] args) {

        int UDPport = 9000;
        int serverPort = 8888;
        Orders ordersForServer = new Orders();
        String inetAddress="192.168.0.103";
        Server server = new Server(serverPort, ordersForServer,inetAddress);
        Client client = new Client(UDPport,inetAddress);
        //Client client2 = new Client(9100);
        //Client client3 = new Client(9500);
        server.threadStart();
       // client.threadStart();
        //client2.threadStart();
        //client3.threadStart();
    }
}
