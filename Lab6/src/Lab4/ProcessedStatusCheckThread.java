package Lab4;

import Lab2.Order;
import Lab2.Orders;
import Lab2.Status;

public class ProcessedStatusCheckThread extends ACheck {

    public ProcessedStatusCheckThread(Orders orders, double seconds) {
        super(orders, seconds);
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
        try {
            while (true) {
                //System.out.println("Изменение состояний в списке");
                for (Order order : orders.getOrderList()) {
                    if (order.getStatus() == Status.WAITING) {
                        order.setStatus(Status.PROCESSED);
                        System.out.println("Состояние изменено");
                    }
                }
                Thread.sleep((long)(1000 * seconds));
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }

    }

}

