package Lab1;

import java.io.Serializable;
import java.util.Scanner;
import java.util.UUID;

public abstract class Product implements ICrudAction,Serializable {
    UUID productID;
    String productName;
    double price;
    static int productCounter;
    String productMakerName;

    Product() {
        //productCounter++;
    }

    public Product(String productName, double price, String productMakerName) {
        this.productID = UUID.randomUUID();
        this.productName = productName;
        this.price = price;
        this.productMakerName = productMakerName;
        productCounter++;
    }

    public abstract void create();

    public abstract void read();

    public UUID getProductID() {
        return productID;
    }

    public void update() {
        Scanner scanner = new Scanner(System.in);
        //System.out.println("Ввод ID-товара ");
        this.productID = UUID.randomUUID();
        System.out.println("Ввод названия товара ");
        this.productName = scanner.next();
        System.out.println("Ввод цены товара ");

        this.price = scanner.nextDouble();
        System.out.println("Ввод фирмы-производителя ");
        this.productMakerName = scanner.next();
    }

    public void delete() {
        //this.productID=;
        this.productName = "";
        this.price = 0;
        productCounter--;
        this.productMakerName = "";

    }
}
