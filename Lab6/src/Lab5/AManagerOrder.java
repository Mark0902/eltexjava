package Lab5;

public abstract class AManagerOrder implements IOrder {
    public String path;
    public String objName;

    public AManagerOrder(String path,String objName) {
        this.path = path;
        this.objName=objName;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public String getObjName() {
        return objName;
    }

    public void setObjName(String objName) {
        this.objName = objName;
    }
}
